\documentclass[10pt]{beamer}
\usetheme{metropolis}

\usepackage{minted}

\AtBeginSection[]{ %
	\begin{frame}
		\vfill{}
		\begin{center}
			\textbf{\LARGE{\insertsectionhead}}
		\end{center}
		\vfill{}
	\end{frame}
}

\title{Constructing Classes \& Other Tools}
\subtitle{CS 311}
\author{Adam Sweeney}
\institute{Wichita State University, EECS}

\begin{document}
\begin{frame}
	\titlepage{}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Introduction}
	\begin{itemize}
		\item We have seen that to initialize a \texttt{struct}, we do something like this
		\begin{minted}[gobble=2, tabsize=4]{c++}
			struct Foo {
				int x;
				double y;
			};

			Foo test = {2, 3.14};
		\end{minted}
		\item How can we initialize a class, if the data is private?
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Overview}
	\begin{itemize}
		\item Constructors
		\item \texttt{const}
		\item \texttt{static}
		\item Inline Functions
		\item Nested and local classes
	\end{itemize}
\end{frame}

\section{Constructors}
\begin{frame}[fragile]
	\frametitle{Constructors}
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{minted}[gobble=4, tabsize=4, linenos]{c++}
				stuct Foo {
					int x;
					double y;
				};

				int main()
				{
					Foo test = {2, 3.14};
					// Program Continues
				}
			\end{minted}
		\end{column}
		\begin{column}{0.1\textwidth}
			% EMPTY FILLER COLUMN
		\end{column}
		\begin{column}{0.4\textwidth}
			\begin{minted}[gobble=4, tabsize=4, linenos]{c++}
				class Foo {
					int x;
					double y;
				};

				int main()
				{
					Foo test = {2, 3.14};
					// Compile Error
			\end{minted}
		\end{column}
	\end{columns}
	\vspace{1em}
	\begin{itemize}
		\item The left side is legal code, but the right side fails to compile. Why?
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Classes Are Private By Default}
	\begin{itemize}
		\item The members of a class are private by default
		\item The initialization that being attempted in \texttt{main()} is not in the class
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{But Encapsulation\dots{}}
	\begin{itemize}
		\item \dots{}means we should make our data private
		\item Recall accessors \& mutators (getters \& setters)
		\item A class member function is able to directly access and manipulate private data members
		\item So what we need is a function that we can call to initialize an object of a class
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Constructors}
	\begin{itemize}
		\item Functions that can initialize an object are called constructors
		\item These are special functions
		\item They are defined a little bit differently than we're used to
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{A Constructor Example}
	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		class DayOfYear {
		public:
			// Constructor
			DayOfYear(int monthValue, int dayValue);
			/*
			 * Other class functions go here
			 */
		private:
			int month;
			int day;
		}
	\end{minted}
\end{frame}

\begin{frame}
	\frametitle{Observations}
	\begin{itemize}
		\item The constructor has the same name as the class
		\item The constructor has no return type specified
		\item The constructor is defined in the public section of the class
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Implementing a Constructor}
	\begin{itemize}
		\item The definition is a little different than what we're used to
		\item Implementing a constructor will also be a little different
		\item Two examples will be shown
		\begin{itemize}
			\item A more familiar method first
			\item The generally preferred (and required in this course) method
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Example the First}
	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		// DayOfYearClass defined above
		DayOfYear::DayOfYear(int monthValue, int dayValue)
		{
			month = monthValue;
			day = dayValue;

			// Validation code runs here
		}
	\end{minted}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Example the Second}
	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		// DayOfYearClass defined above
		DayOfYear::DayOfYear(int monthValue, int dayValue)
			: month(monthValue)
			, day(dayValue)
		{
			// Validation code runs here
		}
	\end{minted}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Side-by-Side}
	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		// DayOfYearClass defined above
		DayOfYear::DayOfYear(int monthValue, int dayValue)
		{
			month = monthValue;
			day = dayValue;

			// Validation code runs here
		}
		/***Only one of these implementations can be used***/
		DayOfYear::DayOfYear(int monthValue, int dayValue)
			: month(monthValue)
			, day(dayValue)
		{
			// Validation code runs here
		}
	\end{minted}
\end{frame}

\begin{frame}
	\frametitle{Initialization Section}
	\begin{itemize}
		\item It is a way to directly initialize class member data
		\item In newer revisions of C++, it can be far more efficient
		\item To keep compilers from whining, the initialization section should handle the variables in the same 
		order they are declared in the class definition
		\item When possible, constructors are required to use the initialization section in this course
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{How To Use Constructors}
	\begin{itemize}
		\item The way we use constructors are different than we typically expect
		\item They are invoked automatically when an object is declared
		\item It is possible to overload constructors, allowing various methods and degrees of object initialization
		\item It is possible to explicitly call a constructor; reserved for specific circumstances and should not be 
		standard behavior
	\end{itemize}
	\begin{minted}[gobble=1, tabsize=4, linenos]{c++}
		// DayOfYear class defined above
		int main()
		{
			DayOfYear date(5, 23);
			DayOfYear holiday;
			holiday = DayOfYear(7, 4);
			// Program continues
	\end{minted}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Declaring an Object With No Parameters}
	\begin{itemize}
		\item<1-> One might think, then, that the following declaration is legal:\\
		\mintinline{c++}{DayOfYear holiday();}
		\item<2-> One would be wrong
		\item<3-> What that looks like is a function prototype, not an object declaration
		\item<3-> When declaring an object with no parameters, we omit the {( )}
		\begin{itemize}
			\item<3-> Recall declaring \texttt{string} objects
		\end{itemize}
		\mintinline{c++}{DayOfYear holiday;}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Some More Information on Explicit Constructor Calls}
	\begin{itemize}
		\item \mintinline{c++}{holiday = DayOfYear(10, 31);}
		\item Can be used as a way to re-initialize an object that has already been declared
		\item The explicit constructor call creates an anonymous object
		\item The anonymous object is then assigned to \texttt{holiday}
		\item This is inefficient code
		\item The concept of an anonymous object is important, however
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Default Constructor}
	\begin{itemize}
		\item A constructor that takes no parameters is also called a ``default constructor''
		\item If you write a class and do not provide any constructors at all, the compiler provides a default 
		constructor
		\item If you provide ANY constructor for your class, the compiler will not provide a default constructor
		\item This limits the usage of your class
		\item It is a good practice to always provide your own default constructor
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Classes Showing Up in Other Classes}
	\begin{itemize}
		\item It is possible to have a class contain a member variable of a different class type
		\item It is also possible for a class member function to return an object of a different class
		\item The same can be said for containing objects of the same class
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Constructors and C++11}
	\begin{itemize}
		\item C++ provides two new features with regard to constructors
		\item Member initializers
		\begin{itemize}
			\item Similar to a function with default parameter values
		\end{itemize}
		\item Constructor delegation
		\begin{itemize}
			\item Allows a constructor to call another constructor
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Member Initializers}
	\begin{columns}
		\begin{column}{0.1\textwidth}
			% EMPTY FILLER COLUMN
		\end{column}
		\begin{column}{0.4\textwidth}
			\begin{minted}[gobble=4, tabsize=4, linenos]{c++}
				class DayOfYear {
				public:
					// PUBLIC STUFF
				private:
					int month = 1;
					int day = 1;
				};
			\end{minted}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{itemize}
				\item Member initializers simply allow us to assign default values to class data members
				\item This may simplify your constructors, but it can also be considered to violate encapsulation
			\end{itemize}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Constructor Delegation}
	\begin{center}
		\begin{minipage}{0.7\textwidth}
			\begin{minted}[gobble=4, tabsize=4, linenos]{c++}
				class DayOfYear {
				public:
					DayOfYear();
					DayOfYear(int monthValue, int dayValue);
					// REMAINING PUBLIC STUFF
				private:
					// PRIVATE STUFF
				};

				DayOfYear::DayOfYear() : DayOfYear(1, 1) { }
			\end{minted}	
		\end{minipage}
	\end{center}
	\begin{itemize}
		\item Constructor delegation can save us from duplicated code
	\end{itemize}
\end{frame}

\section{Other Tools}
\begin{frame}
	\frametitle{Other Tools}
	\begin{itemize}
		\item \texttt{const}
		\item \texttt{static}
		\item Inline functions
		\item Nested \& local functions
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{\texttt{const}}
	\begin{itemize}
		\item \mintinline{c++}{int add(const int x, const int y);}
		\item What \texttt{const} does never changes, but we'll see new applications of it in this class
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Pass by Value vs. Pass by Reference}
	\begin{itemize}
		\item<1-> What is the difference?
		\begin{itemize}
			\item<2-> Pass by reference gives the memory address of what you're passing
			\item<2-> Pass by value creates a copy in memory and gives the copy to your function
		\end{itemize}
		\item<3-> Which is more efficient?
		\begin{itemize}
			\item<4-> Pass by reference
		\end{itemize}
		\item<5-> Is it possible to get the efficiency of pass by reference AND the data protection of pass by 
		value?
		\begin{itemize}
			\item<6-> Yes, by passing a \texttt{const} reference
			\item<6-> It would look like this
			\begin{minted}[gobble=4, tabsize=4, linenos]{c++}
				void foo(const DayOfYear &date);
			\end{minted}
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Protecting a Calling Object}
	\begin{itemize}
		\item Imagine the following two functions:
	\begin{minted}[gobble=2, tabsize=4, linenos, fontsize=\small]{c++}
		class DayOfYear {
		public:
			// Other public functions
			void changeDate(int m, int d);
			void print();
			// The rest of the class
		};
	\end{minted}
	\item Let's see what those calls look like:
	\begin{minted}[gobble=2, tabsize=4, linenos, fontsize=\small]{c++}
		int main()
		{
			DayOfYear holiday(1, 1);
			holiday.changeDate(2, 14);
			holiday.print();
		}
	\end{minted}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Like Passing by Reference}
	\begin{itemize}
		\item It can be inferred from the function names that \texttt{changeDate()} will manipulate the calling 
		object
		\item It can be assumed that \texttt{print()} shouldn't have to modify any class member data
		\item we can guarantee that a function does not alter the calling object with \texttt{const}
		\item It would look like this:
		\begin{minted}[gobble=3, tabsize=4, linenos]{c++}
			class DayOfYear {
			public:
				// Other public functions
				void changeDate(int m, int d);
				void print() const;
				// The rest of the class
			};
		\end{minted} 
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Making an Entire Function \texttt{const}}
	\begin{itemize}
		\item Placing \texttt{const} after the function name becomes part of the definition
		\begin{itemize}
			\item You must also place \texttt{const} when you implement the function
		\end{itemize}
		\item It ensures that the calling object will not be altered by the function
		\item The compiler will enforce this for us
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\texttt{const} Consistency}
	\begin{itemize}
		\item Proper use of \texttt{const} is a best practice
		\item Sporadic use of \texttt{const}, or implementing \texttt{const} after the fact is generally a 
		nightmare
		\item Plan to use it from the beginning, and use it properly
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\texttt{static}}
	\begin{itemize}
		\item Imagine that you run a hot dog stand
		\item If it becomes very successful, you may want to open many hot dog stands
		\item You then may want to franchise it
		\item You then may want to keep track of total hot dog sales, and how many stands you have
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{How to Track Your Hotdog Stands}
	\begin{itemize}
		\item We want to keep this information in the class
		\item But we couldn't track it in the class
		\begin{itemize}
			\item Not without a lot of painful work and manual tracking
		\end{itemize}
		\item But this information is pertinent to the class
		\item Enter \texttt{static}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{What is Even \texttt{static}}
	\begin{itemize}
		\item \texttt{static} is interesting, as it is not a part of any object
		\item But you can only access it through the class
		\item Items declared statically only exist once, across \textbf{all} objects
		\item You cannot use a calling object to access static member data or static member functions
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{\texttt{static} Data Members}
	\begin{itemize}
		\item For example, if I want to keep track of the amount of hotdog stands that I have, I could use a static 
		variable
		\begin{minted}[gobble=3, tabsize=4, linenos]{c++}
			class HotdogStand {
			public:
				// PUBLIC STUFF
			private:
				// PRIVATE STUFF
				static int numStands;
			};
		\end{minted}
		\item Declaring a member variable as \texttt{static} is quite simple
		\item But now let's consider access to this variable
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Accessing a \texttt{static} Data Member}
	\begin{itemize}
		\item \texttt{static} member variables cannot be accessed through a calling object, only through the class
		\item That looks something like this
		\begin{minted}[gobble=3, tabsize=4, linenos]{c++}
			// HotdogStand class with static numStands declared above
			int main()
			{
				HotdogStand first;
				std::cout << HotdogStand::numStands << std::endl;
				// Program continues
			}
		\end{minted}
		\item How would we initialize a \texttt{static} variable, especially one in the private space?
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Initializing \texttt{static} Data Members}
	\begin{itemize}
		\item<1-> It's a bit different
		\item<1-> We can't do it in a constructor, why?
		\item<1-> We can't do it using data member initialization, why?
		\item We actually do it outside the class, even if the \texttt{static} data member is private
		\begin{itemize}
			\item It is allowed to initialize the variable at the point of definition
		\end{itemize}
		\begin{minted}[gobble=3, tabsize=4, linenos]{c++}
			class HotdogStand {
			public:
				// PUBLIC STUFF
			private:
				// PRIVATE STUFF
				static int numStands;
			};

			int HotdogStand::numStands = 0;
		\end{minted}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\texttt{static} Member Access}
	\begin{itemize}
		\item Any member function can see and manipulate \texttt{static} data members
		\item So, if keeping a tally, constructors are a good place to increment a \texttt{static} variable
		\item Because \texttt{static} member variables both do and do not exist in a class using a fully fledged 
		class member function can be overkill
		\begin{itemize}
			\item We would want a getter for a \texttt{static} variable to
			\begin{itemize}
				\item Only have access to the static variable and not a calling object
				\item Exist in the same space as a \texttt{static} variable for consistency's sake
			\end{itemize}
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\texttt{static} Functions}
	\begin{itemize}
		\item \texttt{static} functions exist in the same space as static variables
		\item This means that you cannot use a calling object to invoke them
		\item \texttt{static} functions cannot access non-static private data or functions
		\begin{itemize}
			\item This should make sense, since a \texttt{static} function has no calling object, there are no member 
			variables or functions for it to access
		\end{itemize}
		\item There is one other noteworthy point about \texttt{static} functions
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{But First, An Aside}
	\begin{itemize}
		\item Every object has access to its own address
		\item This is achieved through the keyword \texttt{this}
		\item \texttt{this} returns a pointer to the calling object
		\begin{minted}[gobble=3, tabsize=4, linenos]{c++}
			// using class DayOfYear from previous lecture
			int getMonth()
			{
				return this->month;
				// return (*this).month;
				// return month;
			}
		\end{minted}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Back to \texttt{static} Function Access}
	\begin{itemize}
		\item \texttt{static} functions do \textbf{not} have access to the keyword \texttt{this}
		\begin{itemize}
			\item Again, because there is no calling object for \texttt{static} functions, this should make sense
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Inline Functions}
	\begin{itemize}
		\item These are actually exactly what they sound like
		\item Functions that are implemented inline with their declaration within a class
		\begin{minted}[gobble=3, tabsize=4, linenos]{c++}
			class DayOfYear {
			public:
				// PUBLIC STUFF
				int getMonth() const { return month; }
			private:
				// PRIVATE STUFF
			};
		\end{minted}
		\item Use of inline functions will depend on what you value more
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Pro and Con of Inline Functions}
	\begin{itemize}
		\item Pro
		\begin{itemize}
			\item They can be much more efficient
		\end{itemize}
		\item Con
		\begin{itemize}
			\item They violate encapsulation
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Nested \& Local Classes}
	\begin{itemize}
		\item It is possible to declare a class within a class
		\begin{itemize}
			\item This is a little different than what we're used to, because we cannot declare a new function from 
			within a function
			\item This is known as a nested class
			\item This will be discussed in more detail later in the semester
		\end{itemize}
		\item It is possible to declare a class inside a function
		\begin{itemize}
			\item Because of scope, that class only exists within that function
			\item This means that only that function can create objects of that class type
			\item When the function goes out of scope, so do all objects of the class, and so does the definition
			\item This called a local class
			\item We won't be using any, but it's good to know they exist
		\end{itemize}
	\end{itemize}
\end{frame}

\end{document}