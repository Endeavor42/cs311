\documentclass[10pt]{beamer}
\usetheme{metropolis}

\usepackage{minted}

\AtBeginSection[]{ %
	\begin{frame}
		\vfill{}
		\begin{center}
			\textbf{\LARGE{\insertsectionhead}}
		\end{center}
		\vfill{}
	\end{frame}
}

\title{Structure \& Class}
\subtitle{CS 311}
\author{Adam Sweeney}
\institute{Wichita State University, EECS}

\begin{document}
\begin{frame}
	\titlepage{}
\end{frame}

\begin{frame}
	\frametitle{Introduction}
	\begin{itemize}
		\item Many times, in order to describe something in C++ one variable cannot suffice
		\item Many times, we wish to describe and operate something in C++
		\item C++ provides ways of cleanly handling these situations
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Overview}
	\begin{itemize}
		\item Structures
		\item Classes
		\item Structures vs. Classes
	\end{itemize}
\end{frame}

\section{Structures}
\begin{frame}
	\frametitle{Structures}
	\begin{itemize}
		\item Structures can hold multiple variables under a single name, like an array
		\item Similarities with arrays end there
		\item Structures can hold variables of different types
		\item Accessing these variables is done differently than an array
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Defining a Structure}
	Below is the generic form of a \texttt{struct}

	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		struct [TYPENAME] {
			TYPE VAR01;
			TYPE VAR02;
			.
			.
			.
			TYPE VARNN;
		} [OBJECT01] [OBJECT02] ... [OBJECTNN];
	\end{minted}
	\vspace{1em}
	The parts enclosed in {[ ]} are optional
\end{frame}

\begin{frame}[fragile]
	\frametitle{Typical \texttt{struct} Definition}
	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		struct Album {
			std::string artist;
			std::string title;
			int year;
			int numTracks;
			double rating;
		};
	\end{minted}
	\begin{itemize}
		\item We will prefer to alwyas define a typename for a \texttt{struct}, and define ``typename'' objects in our 
		code
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{When I Provide a Typename}
	\begin{itemize}
		\item I create a new, heterogeneous data type
		\item In the same way I declare variables of type \texttt{int} or \texttt{double}, I can now declare variables 
		of type \texttt{Album}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Declaring a \texttt{struct} Object}
	\begin{itemize}
		\item First, why object and not variable?
		\begin{itemize}
			\item We typically think of variables as holding a single piece of information
			\item Arrays are a bit special, as the multiple variables are still all very tightly connected
			\item We typically call instances of structures and classes objects
		\end{itemize}
		\item Consider the following:
		\begin{minted}[gobble=3, tabsize=4, linenos]{c++}
			// struct Album defined above
			int main()
			{
				Album blue;
				// Program continues
		\end{minted}
		\item Declaring a \texttt{struct} object is as simple as that
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Initializing a \texttt{struct}}
	\begin{itemize}
		\item Many times, we would like to initialize a variable or object
		\item With structures, we do it the same way we would with an array
		\item Consider the following:
		\begin{minted}[gobble=3, tabsize=4, linenos]{c++}
			// struct Album defined above
			int main()
			{
				Album blue = {"Weezer", "The Blue Album", 1994, 10, 5};
				// Program continues
			}
		\end{minted}
		\item It is important to initialize variables in the \textit{same} order they are declared in the 
		\texttt{struct} definition
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Data Members}
	\begin{itemize}
		\item Data Members are what we call the individual variables within a \texttt{struct}
		\begin{itemize}
			\item They are members of the \texttt{struct}, by the fact that they are part of the definition of the 
			\texttt{struct}
			\item Variables hold data
		\end{itemize}
		\item While it's nice that a \texttt{struct} can collect these variables together, we typically want to view or 
		modify the individual variables within a \texttt{struct}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{The Dot Operator}
	\begin{itemize}
		\item Let's say I have the following code
		\begin{minted}[gobble=3, tabsize=4, linenos]{c++}
			// struct Album defined above
			int main()
			{
				Album red;
				red.artist = "Weezer";
				red.title = "The Red Album";
				red.year = 2008;
				red.numTracks = 16;
				red.rating = 4.6;
				// Program continues
			}
		\end{minted}
		\item By typing the name of the \texttt{struct} object, then `.', we are able to access the individual data 
		members of the \texttt{struct} for viewing or modifying
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Assignment to a \texttt{struct}}
	\begin{itemize}
		\item If two structures are of the same type (both Albums, for example), one can be assigned to another 
		directly
		\begin{minted}[gobble=3, tabsize=4, linenos]{c++}
			// struct Album defined above
			int main()
			{
				Album blue = {"Weezer", "The Blue Album", 1994, 10, 5};
				Album cool;

				cool = blue;
				// Program continues
		\end{minted}
		\item This should make sense, as we assign double values to other doubles and so on
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Notes}
	\begin{itemize}
		\item \texttt{blue.artist} is a string object
		\begin{itemize}
			\item This means I could pass \texttt{blue.artist} to a function that takes a string as a parameter
			\item If a function returns a string, I could save it into \texttt{blue.artist}
		\end{itemize}
		\item Similar principles are at play for every data member of a \texttt{struct}
		\item A \texttt{struct} can be passed to a function, or returned by a function
		\item A \texttt{struct} can contain as a data member another \texttt{struct}
		\item We can do things to a \texttt{struct} or with it that we expect from our basic data types
		\item If we use the dot operator to isolate a single variable, we can use that variable in all the ways we 
		expect
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Interacting with a \texttt{struct}}
	\begin{itemize}
		\item As we'll see, we can intereact with a \texttt{struct} in basically the same way we interact with other 
		variables
		\item We may want to create an array of Book structures that represents our personal library
		\item We can write functions to maintain our virtual library
		\item It can get messy
		\item Example time!
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Some Issues With That Program}
	\begin{itemize}
		\item The array \texttt{library} and integer \texttt{numBooksInLibrary} can be altered directly
		\begin{itemize}
			\item If those two variables cannot be trusted, all the functions associated with the library will either 
			break or require a LOT of extra code to ensure they can safely operate
		\end{itemize}
		\item The function \texttt{findBook()} is designed to only be run after \texttt{isInLibrary()}
	\end{itemize}
\end{frame}

\section{Classes}
\begin{frame}
	\frametitle{Classes}
	\begin{itemize}
		\item Classes are strongly preferred over structures when we also need to use functions to view or manipulate 
		the data
		\begin{itemize}
			\item This simply has to do with what our expectations of a \texttt{struct} and \texttt{class} should be
		\end{itemize}
		\item Many times, we may feel it necessary to protect our data, and ensure it is only manipulated a certain way 
		or not at all
		\begin{itemize}
			\item This is as much a security concern as it is making our lives easier
		\end{itemize}
		\item A class is far more portable than a \texttt{struct} definition and a collection of functions
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Classes}
	\begin{itemize}
		\item Classes can hold mulitple variables and functions under a single name
		\item Let's look at a very simple class
	\end{itemize}
	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		class DayOfYear {
		public:
			void output();
			int month;
			int day;
		};
	\end{minted}
	\begin{itemize}
		\item At first glance, not a lot is different
		\begin{itemize}
			\item The class is very simple, after all
		\end{itemize}
		\item Note that there is a function along with two variables
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Accessing Class Members}
	\begin{itemize}
		\item It's the same as a \texttt{struct}; use the dot operator
		\item When accessing a class member function, don't forget the parameters after the function name
		\begin{itemize}
			\item Or an empty set of {( )} if the function takes no parameters
		\end{itemize}
	\end{itemize}
	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		// DayOfYear class defined above
		int main()
		{
			DayOfYear birthday;
			birthday.output();
			// Program continues
	\end{minted}
\end{frame}

\begin{frame}
	\frametitle{The Calling Object}
	\begin{itemize}
		\item Both structures and classes use the dot operator to access the individual variables and/or functions
		\item A \texttt{struct} or \texttt{class} object is \textit{always} on the left side of the dot operator
		\item That object is called the ``calling object''
		\begin{itemize}
			\item It is the object calling \textit{its} copy of the variable or function
		\end{itemize}
		\item The concept of the calling object and what it does and does not have access to is very important
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Before Discussing Access}
	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		class DayOfYear {
		public:
			void output();
			int month;
			int day;
		};
	\end{minted}
	\begin{itemize}
		\item Let's discuss why the class has the ``\texttt{public:}'' line
		\item This takes us to the \textit{only} real difference between a \texttt{struct} and a \texttt{class}
		\item By default, all members of a \texttt{struct} are public, and all members of a class are private
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Public vs. Private}
	\begin{itemize}
		\item Member data and member functions in the public space are accessible to anyone and anything
		\item Member data and member functions in the private space are accessible \textbf{only} within the 
		\texttt{struct} or \texttt{class}
		\item So, the \texttt{DayOfYear} class is behaving like a \texttt{struct} by placing everything in the public 
		secion
		\item The data security mentioned is not present, anyone could arbitrarily change the month or day
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Encapsulation}
	\begin{itemize}
		\item Two things you will hear a lot in the class are:
		\begin{itemize}
			\item Encapsulation
			\item Pillar of Object-Oriented Programming
		\end{itemize}
		\item Encapsulation is also known as \textit{information hiding} or \textit{data abstraction}
		\item It is the concept that the implementation of member functions and member data is irrelevant to the use of 
		the class
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{What Should Be Private?}
	\begin{itemize}
		\item All class data should typically be private
		\item ``Helper'' functions
		\begin{itemize}
			\item These are functions that make the operation of your class easier which you do not want to be made 
			public
			\item An example could be a function that validates a day of the year, ensuring that it always is valid
			\item Sometimes the helper function is a real function that you want the user to use, but you provide a 
			different overload publicly to hide unnecessary details from the user
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Revised \texttt{DayOfYear} Class}
	\begin{itemize}
		\item Below are two examples of implementing the \texttt{DayOfYear} class with private data members
	\end{itemize}
	\begin{columns}
		\begin{column}{0.6\textwidth}
			\begin{minted}[gobble=4, tabsize=4, linenos]{c++}
				class DayOfYear {
				public:
					void output();
				private:
					int month;
					int day;
				};
			\end{minted}
		\end{column}
		\begin{column}{0.4\textwidth}
			\begin{minted}[gobble=4, tabsize=4, linenos]{c++}
				class DayOfYear {
					int month;
					int day;
				public:
					void output();
				};
			\end{minted}
		\end{column}
	\end{columns}
	\begin{itemize}
		\item Recall that a class is private by default
		\item Even so, in this class we will prefer the formatting on the left side
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Explanation of Preferred Formatting}
	\begin{columns}
		\begin{column}{0.6\textwidth}
			\begin{itemize}
				\item This version is preferred because it generally better represents the goals of encapsulation
				\item C++ does not allow the private information to be completely hidden
				\item So we instead show the user what they have access to first, as opposed to showing them private 
				information first
				\item This method is also simpler to read, as it forces both the public and private sections to be 
				labeled
			\end{itemize}
		\end{column}
		\begin{column}{0.1\textwidth}
			% EMPTY FILLER COLUMN
		\end{column}
		\begin{column}{0.3\textwidth}
			\begin{minted}[gobble=4, tabsize=4, linenos]{c++}
				class DayOfYear {
				public:
					void output();
				private:
					int month;
					int day;
				};
			\end{minted}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]
	\frametitle{We Were Talking About Calling Object Access...}
	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		// DayOfYear class defined above w/ private data
		int main()
		{
			DayOfYear birthday;
			birthday.month = 9;    // ILLEGAL NOW
			// Compile error
	\end{minted}
	\begin{itemize}
		\item A calling object has direct access to its private data members through the member functions
		\item It also has access to the private data of any other object of the same class
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{An Example}
	\begin{itemize}
		\item Below is the implementation of the \texttt{DayOfYear} function output
	\end{itemize}
	\begin{minted}[gobble=2, tabsize=4, linenos]{c++}
		void DayOfYear::output()
		{
			std::cout << month << "/" << day;
		}
	\end{minted}
	\begin{itemize}
		\item Because \texttt{output()} is a member function of the class \texttt{DayOfYear}, it has direct access to 
		the private data members \texttt{month} and \texttt{day}
		\item Any other function that is not a part of the \texttt{DayOfYear} class cannot directly invoke the 
		variables \texttt{month} or \texttt{day}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{What is the Deal With That \texttt{::}?}
	\begin{itemize}
		\item Two colons (not semicolons) with no spaces is known as the ``scope resolution operator''
		\item Typically, all of the standard functions in C++ belong to a namespace, usually \texttt{std}
		\item In my code snippets, I am not including the line \texttt{using namespace std;}
		\item This means I have to fully qualify my function names by specifying what namespace the compiler should 
		look in
		\item But why was it necessary in front of \texttt{output()}?
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{The Scope of a Class Function}
	\begin{itemize}
		\item<1-> It exists as a member function of a class object
		\item<1-> It \textit{cannot} exist as an independent function
		\item<1-> The \texttt{DayOfYear} class member function \texttt{output()} is not a function that exists on its 
		own
		\item<1-> Knowing that, why is it still necessary to use the scope resolution operator?
		\item<2-> As our coding gets more complex, we will be using many classes, both standard and custom
		\item<2-> When using the functions, it is unambiguous what class they belong to (calling objects)
		\item<2-> But when implementing them, we need to be very specific
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{What if it's Okay For Users to See Private Data?}
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{itemize}
				\item We can provide functions to give users copies of the data
				\item These are called ``accessors'' or ``getters''
				\item We can return the variable directly, or return a sanitized value, or any other necessary value 
				(such as a calculated one)
			\end{itemize}
		\end{column}
		\begin{column}{0.1\textwidth}
			% EMPTY FILLER COLUMN
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{minted}[gobble=4, tabsize=4, linenos]{c++}
				class DayOfYear {
				public:
					void output();
					int getMonth();
					int getDay();
				private:
					int month;
					int day;
				};

				int DayOfYear::getMonth()
				{
					return month;
				}
			\end{minted}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]
	\frametitle{What if it's Okay For Users to Manipulate Private Data?}
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{itemize}
				\item We can provide functions to allow users to manipulate the private data
				\item These are called ``mutators'' or ``setters''
				\item By forcing the user to go through a function, we are able to validate their change and ensure 
				that our object remains valid
			\end{itemize}
		\end{column}
		\begin{column}{0.1\textwidth}
			% EMPTY FILLER COLUMN
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{minted}[gobble=4, tabsize=4, linenos]{c++}
				class DayOfYear {
				public:
					void output();
					int getMonth();
					int getDay();
					void setMonth(int m);
					void setDay(int d);
				private:
					int month;
					int day;
				};

				void DayOfYear::setMonth(int m)
				{
					if (m >= 1 && m <= 12)
						month = m;
				}
			\end{minted}
		\end{column}
	\end{columns}
\end{frame}

\section{Structures vs. Classes}
\begin{frame}
	\frametitle{Structures vs. Classes}
	\begin{itemize}
		\item This is an interesting topic
		\item There is really only a single difference
		\begin{itemize}
			\item Members of a \texttt{struct} are public by default
			\item Members of a \texttt{class} are private by default
		\end{itemize}
		\item Syntactically, everything we did with a \texttt{class}, we can do with a \texttt{struct}
		\item So why do we have both?
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{A Brief History Lesson}
	\begin{itemize}
		\item C++ is what its name implies, C with some extra stuff
		\item That extra stuff is essentially classes
		\item C has structures, and lacked a solid concept of encapsulation in its overall philosophy
		\item Had C++ redefined how a \texttt{struct} behaves, it would have broken compatibility with C programs
		\item So, the keyword \texttt{class} was introduced, and it behaves exactly like a \texttt{struct}, except 
		that it is private by default
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{How Are They Used in C++?}
	\begin{itemize}
		\item We would use a \texttt{struct} if it was acceptable for all the data (and functions) to be public
		\item In this class, we will be dealing with classes from this point on
	\end{itemize}
\end{frame}

\end{document}