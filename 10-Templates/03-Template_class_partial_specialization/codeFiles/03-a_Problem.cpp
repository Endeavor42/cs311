#include <iostream>

// A template function
template <typename T>
void show(T arr[], size_t size)
{
    for (size_t i = 0; i < size; i++)
        std::cout << arr[i] << " ";
}

// A full template specialization for the function show()
template <>
void show<char>(char arr[], size_t size)
{
    for (size_t i = 0; i < size; i++)
        std::cout << arr[i];
}

int main()
{
    int a[10];

    int num = 1;
    for (auto &i : a) {
        i = num;
        ++num;
    }
    show(a, 10);
    std::cout << "\n\n";

    char c[10] = "Hello";
    show(c, 10);
    std::cout << "\n\n";

    // A new case to consider
    num = 0;
    int * p[10];
    for (auto &i : p) { 
        i = new int(num + 1);
        ++num;
    }
    show(p, 10);
    std::cout << std::endl;

    // clean up p
    for (auto &i : p) {
        delete i;
    }

    // No delete [] p command because the array was not allocated from the heap

    return 0;
}

/*
 * As we can see in the third array of type int*, the behavior again isn't what 
 * we expected. 
 * 
 * We could create another specialization for int*, but then we'd feel 
 * obligated to provide one for a pointer to any type. 
 * 
 * You might be thinking, why not specialize T* since that will take care of all
 * pointers to a type? That's a great idea, but C++ does not allow this 
 * FOR FUNCTIONS. A specialization to just T* is called a partial 
 * specialization, since T is still a template parameter. And unfortunately, 
 * I cannot provide a concrete example of why this can't be done for a 
 * function. It can, howerver, be done for a class.
 */