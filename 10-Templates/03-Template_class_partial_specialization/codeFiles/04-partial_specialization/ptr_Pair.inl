/*
 * This file contains a partial specialization for T*
 * This means that T can still be whatever I need it to be, but this class is 
 * used if I create a Pair object where the type is a pointer to some type T
 */

template <typename T>
class Pair<T*> {
public:
    Pair();
    Pair(T* one, T* two);
    Pair(const Pair &cpy);
    Pair(Pair &&mov);
    ~Pair();
    Pair<T*>& operator=(const Pair &rhs);
    Pair<T*>& operator=(Pair &&rhs);
    friend std::ostream& operator <<(std::ostream& sout, const Pair &rhs)
    {
        return sout << "(" << *(rhs.first) << ", " << *(rhs.second) << ")";
    }
private:
    T* first;
    T* second;
};

/*
 * We can see that the friend function was treated a bit differently here
 * The short story is that friends and templates are weird, so the best way to 
 * avoid weirdness is to just inline the friend function.
 */


// Partial specialization implementation
template <typename T>
Pair<T*>::Pair() : first(new T()), second(new T()) { }

template <typename T>
Pair<T*>::Pair(T* one, T* two) : first(new T(*one)), second(new T(*two)) { }

template <typename T>
Pair<T*>::Pair(const Pair &cpy) : first(new T(*(cpy.first))), 
                                  second(new T(*(cpy.second)))
{
}

template <typename T>
Pair<T*>::Pair(Pair &&mov) : first(mov.first), second(mov.second)
{
    mov.first = nullptr;
    mov.second = nullptr;
}

template <typename T>
Pair<T*>::~Pair()
{
    if (first != nullptr)
        delete first;
    
    if (second != nullptr)
        delete second;
    
    first = nullptr;
    second = nullptr;
}

template <typename T>
Pair<T*>& Pair<T*>::operator=(const Pair &rhs)
{
    if (this != &rhs) {
        if (first != nullptr)  { delete first; }
        if (second != nullptr) { delete second; }

        first = new T(*(rhs.first));
        second = new T(*(rhs.second));
    }

    return *this;
}

template <typename T>
Pair<T*>& Pair<T*>::operator=(Pair &&rhs)
{
    if (this != &rhs) {
        if (first != nullptr)  { delete first; }
        if (second != nullptr) { delete second; }

        first = rhs.first;
        second = rhs.second;

        rhs.first = nullptr;
        rhs.second = nullptr;
    }

    return *this;
}
