#include <iostream>

template<class T>
void swap(T& a, T& b);

int main()
{
	char x = 88, y = 94;
	double p = 3.14, g0 = 7.748e-5;

	std::cout << "Integers: " << x << ", " << y << std::endl;
	std::cout << "Doubles: " << p << ", " << g0 << std::endl;
	swap(x, y);
	swap(p, g0);
	std::cout << "Integers: " << x << ", " << y << std::endl;
	std::cout << "Doubles: " << p << ", " << g0 << std::endl;

	// This will work with (almost) any type I throw at it, including our own objects
	// It will (mostly) work with our own objects because either shallow copy suffices, 
	// or we have overloaded the = operator for appropriate deep copy
	return 0;
}

// The compiler will provide only the necessary overloads for the function to work
// So, end result is almost the same, but we write less code and get greater flexibility
template<class T>
void swap(T& a, T& b)
{
	T temp;

	temp = a;
	a = b;
	b = temp;
}
