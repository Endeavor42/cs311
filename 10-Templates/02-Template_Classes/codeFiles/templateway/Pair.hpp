#ifndef PAIRCLASS_HPP
#define PAIRCLASS_HPP

#include <iostream>

template<class T>
class Pair {
public:
	Pair();
	Pair(T a, T b);
	void print() const;
	void swap();
private:
	T first;
	T second;
};

#include "Pair.inl"

#endif