#include <iostream>
#include <string>
#include "Pair.hpp"

int main()
{
	Pair<int> ipair(3, 8);
	Pair<double> dpair(3.0, 8.0);
	Pair<std::string> spair("Hello", "World");

	ipair.print();
	ipair.swap();
	ipair.print();

	std::cout << "\n\n";

	dpair.print();
	dpair.swap();
	dpair.print();
	
	std::cout << "\n\n";
	
	spair.print();
	spair.swap();
	spair.print();

	return 0;
}