#include <iostream>

// A division observer
class DivObserver
{
public:
    DivObserver(int div) : m_div(div) { }
    void update(int val)
    {
        std::cout << val << " div " << m_div << " is " << val / m_div << '\n';
    }
private:
    int m_div;
};

// A modulo observer
class ModObserver
{
public:
    ModObserver(int mod) : m_mod(mod) { }
    void update(int val)
    {
        std::cout << val << " mod " << m_mod << " is " << val % m_mod << '\n';
    }
private:
    int m_mod;
};

/*
 * This is the subject we wish to observe
 * Observe that when a Subject object sets a value, it calls its notify() function
 * The notify function tells the observers to update
 * 
 * There is a large shortcoming:
 *  - The observers are hard-coded into the class (in multiple places), so making changes to the observers requires 
 *    fiddling with the Subject class itself, which is not ideal
 */
class Subject
{
public:
    Subject() : m_div_obj(4), m_mod_obj(3) {}
    void set_value(int value)
    {
        m_value = value;
        notify();
    }
    void notify()
    {
        m_div_obj.update(m_value);
        m_mod_obj.update(m_value);
    }
private:
    int m_value;
    DivObserver m_div_obj;
    ModObserver m_mod_obj;
};


int main()
{
    Subject subj;
    subj.set_value(14);

    return 0;
}