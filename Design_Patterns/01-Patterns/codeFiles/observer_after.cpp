#include <iostream>
#include <vector>

// An interface for all of our Observers
class Observer
{
public:
    virtual void update(int value) = 0;
};

/*
 * Our new Subject class
 * We can see that the set_val() function still calls the notify() function, BUT we no longer have hard-coded 
 * observers in our class
 * 
 * We instead keep a vector of pointers to Observers, and our notify() function simply cycles through the vector and 
 * tells each observer, whatever it may be, to update itself
 * 
 * The attach() function is new to the class, and it's how we add observers to an object
 */
class Subject
{
public:
    void attach(Observer *obs)
    {
        m_views.push_back(obs);
    }
    void set_val(int value)
    {
        m_value = value;
        notify();
    }
    void notify()
    {
        for (unsigned int i = 0; i < m_views.size(); ++i)
            m_views[i]->update(m_value);
    }
private:
    int m_value;
    std::vector<Observer*> m_views;
};

/*
 * Our observers now inherit from the base Observer class (they inherit a common interface)
 * We can also see in their constructor that they are given a Subject, and the Observer "attaches" itself to the 
 * Subject
 * 
 * Each subject is now able to have as many or as few Observers as we want
 */
class DivObserver: public Observer
{
public:
    DivObserver(Subject *model, int div)
    {
        model->attach(this);
        m_div = div;
    }
    void update(int v) override
    {
        std::cout << v << " div " << m_div << " is " << v / m_div << '\n';
    }
private:
    int m_div;
};

class ModObserver: public Observer
{
public:
    ModObserver(Subject *model, int mod)
    {
        model->attach(this);
        m_mod = mod;
    }
    void update(int v) override
    {
        std::cout << v << " mod " << m_mod << " is " << v % m_mod << '\n';
    }
private:
    int m_mod;
};

/*
 * We can see that we are able to attach three observers to a single subject. We are also able to more easily specify 
 * the key values of each Observer. This would have required a lot more code in our Subject class to handle properly in 
 * the before example. But now we no longer need to tweak our Subject class to attach more or fewer Observers. This 
 * makes our code much easier to maintain.
 */
int main()
{
    Subject subj;
    DivObserver divObs1(&subj, 4);
    DivObserver divObs2(&subj, 3);
    ModObserver modObs3(&subj, 3);
    subj.set_val(14);

    return 0;
}