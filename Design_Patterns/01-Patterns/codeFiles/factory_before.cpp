#include <iostream>
#include <vector>

class Stooge
{
public:
    virtual void slap_stick() = 0;

    virtual ~Stooge() {}
};

class Larry: public Stooge
{
public:
    void slap_stick()
    {
        std::cout << "Larry: poke eyes\n";
    }

    ~Larry() {}
};

class Moe: public Stooge
{
public:
    void slap_stick()
    {
        std::cout << "Moe: slap head\n";
    }

    ~Moe() {}
};

class Curly: public Stooge
{
public:
    void slap_stick()
    {
        std::cout << "Curly: suffer abuse\n";
    }

    ~Curly() {}
};


/*
 * We can see in the main() function that the programmer is responsible for constructing objects. That may not be 
 * desirable for us
 */
int main()
{
    std::vector<Stooge*> roles;
    int choice;

    while (true)
    {
        std::cout << "Larry(1) Moe(2) Curly(3) Go(0): ";
        std::cin >> choice;
        if (choice == 0)
            break;
        else if (choice == 1)
            roles.push_back(new Larry);
        else if (choice == 2)
            roles.push_back(new Moe);
        else
            roles.push_back(new Curly);
    }
    for (unsigned int i = 0; i < roles.size(); i++)
        roles[i]->slap_stick();
    for (unsigned int i = 0; i < roles.size(); i++)
        delete roles[i];


    return 0;
}