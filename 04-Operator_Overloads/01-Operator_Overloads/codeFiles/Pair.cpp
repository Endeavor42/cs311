#include <iostream>
#include "Pair.hpp"

namespace {
    void HI();
};

namespace weirdStuff {
    Pair::Pair()
        : Pair(0, 'A')
    {
    }

    Pair::Pair(int f, char s)
        : first(f)
        , second(s)
    {
    }

    int Pair::getFirst() const
    {
        return first;
    }

    char Pair::getSecond() const
    {
        return this->second;
    }

    // void Pair::print() const
    // {
    //     std::cout << "(" << first << ", " << second << ")";
    // }

    const Pair Pair::operator+(const Pair &rhs) const
    {
        /*
        * Rules for addition:
        * The integer part is the sum of the value of the character on the right-hand side is added to the 
        * integer value on the left side
        * The character is replaced by an upper case letter corresponding to the integer on the right hand 
        * side, mod 26
        * Example:
        *      (34, B) + (78, c) = ((34 + 99(c)), 78 % 26 -> upper case letter) -> (133, A)
        */
        int i = this->first + static_cast<int>(rhs.second);
        char c = static_cast<char>((rhs.first % 26) + 65);
        HI();

        return Pair(i, c);
    }

    const Pair operator-(const Pair &lhs, const Pair &rhs)
    {
        /*
        * Rules for subtraction
        * The integer part is the difference between the left operand's integer and the integer value of the 
        * right-hand side's character
        * The character part is replaced by a lower case letter corresponding to the integer on the left hand 
        * side, mod 26
        * Example:
        *  (34, 8) - (78, c) = ((34 - 99(c)), 34 % 26 -> lower case letter) -> (-65, i)
        */

        int i = lhs.getFirst() - static_cast<int>(rhs.getSecond());
        char c = static_cast<char>((lhs.getFirst() % 26) + 97);

        return Pair(i, c);
    }

    std::ostream& operator<<(std::ostream& outS, const Pair& rhs)
    {
        return outS << "(" << rhs.first << ", " << rhs.second << ")";
    }
};

namespace {
    void HI()
    {
        std::cout << "HI\n";
    }
};