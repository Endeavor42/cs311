#include <iostream>
#include <random>
#include <string>
#include <vector>

class Dog {
public:
    Dog();
    Dog(std::string name);

    /* Nested public class/struct declaration
     * Still only a definition, an actual object is held privately
     *
     * Because declared publicly, possible to declare objects of type Dog::Stick outside of class
     */
    struct Stick {
        Stick(std::string w, double l);
        std::string wood_type;
        double length;
    };

    std::string getName() const;
    Stick getStick() const;
    void setStick(Stick s);
private:
    std::string name;
    Stick favorite;     // Stick object as private data
};

int main()
{
    std::vector<Dog::Stick> sticks;
    sticks.emplace_back("Oak", 5.6);        // structs can have constructors, the only difference a struct and class is the default state of members
    sticks.emplace_back("Beech", 7.6);
    sticks.emplace_back("Cherry", 4.7);

    Dog::Stick birch = {"Birch", 26.2};     // Declaration of a Dog::Stick object; scoping is necessary because Stick only exists within Dog (whoah)
    sticks.push_back(birch);

    std::random_device seed;
    std::mt19937 engine(seed());
    std::uniform_int_distribution<int> dist(0, sticks.size() - 1);

    Dog fido("Fido");
    fido.setStick(sticks.at(dist(engine)));

    std::cout << fido.getName() << "'s favorite stick is a(n) " << fido.getStick().wood_type << " branch that is " << fido.getStick().length << " inches long.\n";

    return 0;
}


    Dog::Dog() : name("Dog"), favorite(Stick("Elm", 5.5)) { }

    Dog::Dog(std::string name) : name(name), favorite("Elm", 5.5) { }

    Dog::Stick::Stick(std::string w, double l) : wood_type(w), length(l) { }    // Note the scoping; any Stick function would have to be scoped like this

    std::string Dog::getName() const
    {
        return name;
    }

    // Return type must be scoped; Stick only exists within Dog
    Dog::Stick Dog::getStick() const
    {
        return favorite;
    }

    void Dog::setStick(Stick s)
    {
        favorite = s;
    }