#include <iostream>
#include <string>
#include "Employee.hpp"
#include "HourlyEmployee.hpp"

int main()
{
	companyEmployees::Employee sarah("Sarah Smith", "555-55-5555");
	companyEmployees::HourlyEmployee lynn("Lynn Smith", "555-55-5555", 12.00, 40);

	sarah.printCheck();
	std::cout << "\n##############################\n";
	lynn.printCheck();
	std::cout << "\n##############################\n";
	lynn.Employee::printCheck();
	return 0;
}