#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

struct Money {
	int dollars;
	int cents;
};

struct Account {
	Money balance;
	double rate;
	std::string name;
};

// Functions to do with Accounts and Money
Account createAccount(std::string filename);
Account createAccount(std::string name, double rate, double balance);
Account deposit(Account account, Money deposit);
Money withdraw(Account &account, Money withdraw);
void accrue(Account &account);
void print(Money money);
void print(Account acc);

// TEST HELPER FUNCTIONS -- DO NOT ALTER
int makePennies(Money m);
Money makeMoney(int p);
bool isNegative(Money m);
bool operator<(const Money& lhs, const Money& rhs);

// TEST FUNCTIONS -- DO NOT ALTER
bool testCreateAccountFile(bool n);
bool testCreateAccountParams();
bool testDeposit(Money dep);
bool testWithdraw(Money w);
bool testAccrue();
void testPrint(bool type, bool sign);

int main()
{
	// DO *NOT* ALTER THE MAIN FUNCTION
	int width = 50;
	char prev = std::cout.fill('.');
	std::cout << "\n--- Homework 1 Tests ---\n";
	std::cout << std::setw(width) << std::left << "createAccount(filename)" << testCreateAccountFile(true) << std::endl;
	std::cout << std::setw(width) << std::left << "createAccount(bad filename)" << testCreateAccountFile(false) << std::endl;
	std::cout << std::setw(width) << std::left << "createAccount(parameters)" << testCreateAccountParams() << std::endl;
	std::cout << "\n--- Begin Depost Testing ---\n";
	std::cout << std::setw(width) << std::left << "Deposit Message"; bool temp = testDeposit({10, 0});
	std::cout << std::setw(width) << std::left << "deposit()" << temp << std::endl;
	std::cout << std::setw(width) << std::left << "\nDeposit Message"; temp = testDeposit({0, -75});
	std::cout << std::setw(width) << std::left << "deposit() (negative deposit)" << temp << std::endl;
	std::cout << "--- End Deposit Testing ---\n\n";
	std::cout << "\n--- Begin Withdraw Testing---\n";
	std::cout << std::setw(width) << std::left << "Withdrawl Message"; temp = testWithdraw({10, 36});
	std::cout << std::setw(width) << std::left << "withdraw()" << temp << std::endl;
	std::cout << std::setw(width) << std::left << "\nWithdrawl Message"; temp = testWithdraw({60, 10});
	std::cout << std::setw(width) << std::left << "withdraw() (partial overdraft)" << temp << std::endl;
	std::cout << std::setw(width) << std::left << "\nWithdrawl Message"; temp = testWithdraw({200, 78});
	std::cout << std::setw(width) << std::left << "withdraw() (full overdraft)" << temp << std::endl;
	std::cout << std::setw(width) << std::left << "\nWithdrawl Message"; temp = testWithdraw({-3, -50});
	std::cout << std::setw(width) << std::left << "withdraw() (negative withdraw)" << temp << std::endl;
	std::cout << "--- End Withdraw Testing---\n\n";
	std::cout << "\n--- Begin Accrue Test ---\n";
	std::cout << std::setw(width) << std::left << "Accrue Message"; temp = testAccrue(); std::cout << std::endl;
	std::cout << std::setw(width) << std::left << "accrue()" << temp << std::endl;
	std::cout << "--- End Accrue Test ---\n\n";
	std::cout << std::setw(width) << std::left << "print() (Money) [Expect $567.32]"; testPrint(false, true); std::cout << std::endl;
	std::cout << std::setw(width) << std::left << "print() (Money) (negative) [Expect ($567.32)]"; testPrint(false, false); std::cout << std::endl;
	std::cout << std::setw(width) << std::left << "print() (Account) [Expect $567.32]"; testPrint(true, true); std::cout << std::endl;
	std::cout << std::setw(width) << std::left << "print() (Account) (negative) [Expect ($567.32)]"; testPrint(true, false); std::cout << std::endl;
	/*
	Account createAccount(std::string filename);
	Account createAccount(std::string name, double rate, double balance);
	Account deposit(Account account, Money deposit);
	Money withdraw(Account &account, Money withdraw);
	void accrue(Account &account);
	void print(Money money);
	void print(Account acc);
	*/

	std::cout.fill(prev);

	return 0;
}

// IMPLEMENT YOUR FUNCTIONS HERE
Account createAccount(std::string filename)
{
	Account t;
	double inputMoney;
	std::ifstream filein(filename.c_str());
	if (filein.fail()) {
		return Account{makeMoney(100 * 100), 0.01, "Savings"};
	}
	
	std::getline(filein, t.name);
	filein >> t.rate;
	filein >> inputMoney;

	int balPennies = round(inputMoney * 100);
	t.balance = makeMoney(balPennies);

	filein.close();

	return t;
}

Account createAccount(std::string name, double rate, Money balance)
{
	return  Account{balance, rate, name};
}

Account deposit(Account account, Money deposit)
{
	if (deposit.dollars < 0 || deposit.cents < 0) {
		std::cout << "Cannot make negative deposit.\n";
		return account;
	}

	Account t = account;
	int balPennies = makePennies(t.balance);
	int depPennies = makePennies(deposit);
	int sumPennies = balPennies + depPennies;

	t.balance = makeMoney(sumPennies);

	print(deposit);
	std::cout << " deposited into " << t.name << std::endl;

	return t;
}

Money withdraw(Account &account, Money withdraw)
{
	/***** HINT - THE ISSUE ISN'T IN THIS FUNCTION *****/
	int balPennies = makePennies(account.balance);
	int withPennies = makePennies(withdraw);
	if (withPennies < 0) {
		Money m = {0, 0};
		std::cout << "Cannot make negative withdrawl.\n";
		print(m);
		std::cout << " withdrawn from " << account.name << ".\n";
		return m;
	}

	if (balPennies - withPennies < -5000) {
		int partPennies = balPennies + 5000;
		Money partial = makeMoney(partPennies);

		int newBalPennies = balPennies - partPennies;
		account.balance = makeMoney(newBalPennies);

		print(partial);
		std::cout << " withdrawn from " << account.name << ".\n";
		return partial;
	}

	account.balance = makeMoney((balPennies - withPennies));
	print(withdraw);
	std::cout << " withdrawn from " << account.name << ".\n";

	return withdraw;
}

void accrue(Account &account)
{
	int balPennies = makePennies(account.balance);
	int interestPennies = round(static_cast<double>(balPennies) * account.rate);
	balPennies += interestPennies;

	account.balance = makeMoney(balPennies);
	std::cout.setf(std::ios::fixed);
	std::cout.setf(std::ios::showpoint);
	std::cout.precision(2);
	std::cout << "\nAt " << (account.rate * 100) << "%, your " << account.name
				<< " account earned ";
	Money i = makeMoney(interestPennies);
	print(i);
	std::cout << ".\n";
}

void print(Money money)
{
	char prev = std::cout.fill('0');

	if (isNegative(money))
		std::cout << "(";

	std::cout << "$" << abs(money.dollars) << ".";

	if (abs(money.cents) < 10)
		std::cout.width(2);
	std::cout << abs(money.cents);

	if (isNegative(money))
		std::cout << ")";
	std::cout.fill(prev);
}

void print(Account acc)
{
	print(acc.balance);
}

int makePennies(Money m)
{
	return (m.dollars * 100) + m.cents;
}

Money makeMoney(int p)
{
	return Money{p / 100, p % 100};
}

bool isNegative(Money m)
{
	if (m.dollars < 0 || m.cents < 0)
		return true;
	else
		return false;
}

bool operator<(const Money& lhs, const Money& rhs)
{
	int leftPennies = makePennies(lhs);
	int rightPennies = makePennies(rhs);

	return leftPennies < rightPennies;
}
// DO *NOT* ALTER THESE FUNCTION IMPLEMENTATIONS
bool testCreateAccountFile(bool n)
{
	std::string fn;
	if (n)
		fn = "inputs";
	else
		fn = "garbage";
	
	Account acc = createAccount(fn);

	if (acc.name == "Saving for college" && acc.rate == 0.01 && acc.balance.dollars == 4321 && acc.balance.cents == 98)
		return true;
	else if (fn == "garbage" && acc.name == "Savings" && acc.rate == 0.01 && acc.balance.dollars == 100 && acc.balance.cents == 0)
		return true;
	else
		return false;
}

bool testCreateAccountParams()
{
	Account t = createAccount("Roth IRA", 0.055, {1234, 56});
	if (t.name == "Roth IRA" && t.rate == 0.055 && t.balance.dollars == 1234 && t.balance.cents == 56)
		return true;
	else
		return false;
}

bool testDeposit(Money dep)
{
	Account t = createAccount("Checking", 0.005, {50, 0});
	int dollars = dep.dollars;
	int cents = dep.cents;

	t = deposit(t, dep);

	if (t.balance.dollars == 50 + dollars && t.balance.cents == 0 + cents) {
		return true;
	} else if (isNegative(dep)) {
		if (t.balance.dollars == 50 && t.balance.cents == 0) {
			return true;
		}
	} else {
		return false;
	}

	return false;
}

// USE AS DEBUGGER EXAMPLE
bool testWithdraw(Money w)
{
	Account t = createAccount("Index Fund", 0.06, {50, 0});
	Money overdrawLimit{t.balance.dollars + 50, t.balance.cents};

	Money amount = withdraw(t, w);

	if (w < t.balance) {
		if (amount.dollars == 0 && amount.cents == 0) {
			return true;
		}
		return false;
	} else if (w < overdrawLimit) {
		if (w.dollars == amount.dollars && w.cents == amount.cents) {
			return true;
		}
		return false;
	} else if (isNegative(w)) {
		if (w.dollars == amount.dollars && w.cents == amount.cents) {
			return true;
		}
		return false;
	} else {
		if (amount.dollars == overdrawLimit.dollars && amount.cents == overdrawLimit.cents) {
			return true;
		}
		return false;
	}

	return false;
}

bool testAccrue()
{
	Account t = {{48, 31}, 0.02, "Savings"};

	accrue(t);

	if (t.balance.dollars == 49 && t.balance.cents == 28)
		return true;
	else
		return false;
}

void testPrint(bool type, bool sign)
{
	Account a;
	Money m;

	if (type) {
		if (sign) {
			a = createAccount("CD", 0.03, {567, 32});
		} else {
			a = createAccount("CD", 0.03, {-567, -32});
		}
		print(a);
		return;
	} else {
		if (sign) {
			m = {567, 32};
		} else {
			m = {-567, -32};
		}
		print(m);
		return;
	}
}
