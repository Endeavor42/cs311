#include <iostream>
#include <string>

class Response {
public:
    Response() : reaction("whatever") { }
    Response(std::string r) : reaction(r) { }
    std::string getReaction() const { return reaction; }
private:
    std::string reaction;
};

void reaction(int r);

int main()
{
    int input;

    
    std::cout << "On whatever scale, how are you feeling? ";
    std::cin >> input;

    try {
        reaction(input);
    }
    catch(Response r) {
        std::cout << "I'm feeling " << r.getReaction() << std::endl;
    }
    catch(...) { }

    return 0;
}

// Can throw Response exceptions
void reaction(int r)
{
    if (r < 0)
        throw Response("angry");
    else if (r > 0)
        throw Response("copesetic");
    else
        throw Response("meh");
}