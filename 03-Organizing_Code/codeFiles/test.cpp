//Name: Macie Hamlett
//WSUID: a756f638
// HW#: Homework 2
// Description: A program that will calculate the remaining balance of a loan.

#include <iostream>
#include <iomanip>

using namespace std; 

int main()
{
    //defining the variables 
    double loan_amount;
    double interest_rate;
    double monthly_payment;

    //Displays the decimal places correctly 
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    cout << "\n";
    cout << "Enter amount of loan: ";
    cin >> loan_amount;
    cout << "Enter interest rate: ";
    cin >> interest_rate;
    cout << "Enter monthly payment: ";
    cin >> monthly_payment; 
    cout << "\n";

    for (int i = 1; i < 4; i++)
    {
        //char prev = cout.fill(' ');
        cout << "Year " <<  i  << setw(15)  <<  "Interest"  <<  setw(15)  <<  "Principal"  <<  endl;
        //cout.fill(prev);

        for(int j = 1; j < 13; j++) //formatting the program
        {
            //char prev = cout.fill(' ');
            cout << "------" << setw(15) << "--------" << setw(15) << "---------" << endl;
            // cout.fill(prev);

            for (int counter = 1; counter < 13; counter++)
            {
                cout << counter << ":" << endl;
                cout << setw(15) << (loan_amount * interest_rate) / 12 << endl;
            }
            return 0;
        }
    }
return 0;
}