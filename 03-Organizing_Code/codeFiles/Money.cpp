#include <cmath>
#include <cstdlib>
#include <iostream>
#include "Money.hpp"

// namespace MyAwesomeBusiness {
	Money::Money()
		: dollars(0)
		, cents(0)
	{
	}

	Money::Money(int d, int c)
		: dollars(d)
		, cents(c)
	{
		validate();
	}

	Money::Money(int d)
		: dollars(d)
		, cents(0)
	{
	}

	Money::Money(double m)
	{
		int pennies = round(m * 100);
		dollars = pennies / 100;
		cents = pennies % 100;
	}

	int Money::getPennies() const
	{
		return makePennies();
	}

	bool Money::isNegative() const
	{
		if (dollars < 0 || cents < 0)
			return true;
		else
			return false;
	}

	void Money::add(const Money &m)
	{
		int left = makePennies();
		int right = m.makePennies();
		int sum = left + right;

		dollars = sum / 100;
		cents = sum % 100;
	}

	void Money::subtract(const Money &m)
	{
		Money temp(m.dollars * -1, m.cents * -1);
		add(temp);
	}

	bool Money::isEqual(const Money &m) const
	{
		if (dollars == m.dollars && cents == m.cents)
			return true;
		else
			return false;
	}

	void Money::print() const
	{
		if (dollars < 0 || cents < 0)
			std::cout << "(";
		
		std::cout << "$" << abs(dollars) << ".";
		if (abs(cents) < 10)
			std::cout << "0";
		std::cout << abs(cents);

		if (dollars < 0 || cents < 0)
			std::cout << ")";
	}

	int Money::makePennies() const
	{
		return (dollars * 100) + cents;
	}

	void Money::validate()
	{
		if (dollars < 0 && cents > 0)
			cents *= -1;
		else if (cents < 0 && dollars > 0)
			dollars *= -1;
	}

	std::ostream& operator<<(std::ostream& out, const Money& rhs)
	{
		out << "$" << rhs.dollars << ".";
		if (rhs.cents < 10)
			out << "0";
		out << rhs.cents;

		return out;
	}
// }

