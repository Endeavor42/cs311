#include <iostream>
#include "Money.hpp"

/*
 * This short program just exists to demonstrate that addition does indeed work by calling 
 * the getPennies() function in the unnamed namespace
 */
int main()
{
    namespace MMM = MoneyMoneyMoney;
    // using namespace MMM;         // This would be an acceptable use of a using directive, since it is scoped
    using std::cout;    // Using declarations
    using std::endl;    // 

    MMM::Money m(3, 27);
    MMM::Money k(74, 39);

    MMM::Money sum = m + k;

    //Money sum = 5 + m;

    cout << sum << endl;   

    return 0;
}