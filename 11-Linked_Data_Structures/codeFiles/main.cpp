#include <iostream>
#include "Queue.hpp"

template <typename T>
void print(Queue<T> &q);

int main()
{
    Queue<int> iq;

    iq.push(5);
    iq.push(7);
    iq.push(9);
    print(iq);
    
    iq.pop();
    std::cout << std::endl;
    print(iq);
    
    iq.front() = 4;
    std::cout << std::endl;
    print(iq);

    iq.back() = 24;
    iq.push(5);
    std::cout << std::endl;
    print(iq);

    return 0;
}

template <typename T>
void print(Queue<T> &q)
{
    // for (int i : q)
    //     std::cout << i << std::endl;

    /*
     * You can comment between the two ways of using iterators to see that they behave the same
     * You may also find it beneficial to step through the bottom version with the debugger to see how the program 
     * executes
     */


    for (typename Queue<T>::iterator it = q.begin(); it != q.end(); it++)
        std::cout << *it << std::endl;
}