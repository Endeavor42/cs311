#ifndef QUEUE_HPP_
#define QUEUE_HPP_

#include <stdexcept>

template <class T>
class Queue {
public:
    Queue();
    Queue(T n);
    ~Queue();

    class iterator;         // iterator class forward declared here
    iterator begin();       // NOTE that begin() and end() are Queue functions
    iterator end();         // It's because I want the begining and ending of a Queue

    bool empty() const;
    unsigned int size() const;
    T& front();
    const T& front() const;
    T& back();
    const T& back() const;
    void push(T n);
    void pop();
private:
    class Node {
    public:
        Node(T n);
        T& getData();
        const T getData() const;
        Node* getNext() const;
        Node* getPrev() const;
        void setData(T d);
        void setNext(Node *n);
        void setPrev(Node *n);
    private:
        T data;

        Node *next;
        Node *prev;
    };

    Node *_front;
    Node *_back;
    unsigned int _size;
};

/*
 * This is the definition of the iterator class.
 * There are two reasons for forward declaring in the Queue class and not placing the definition inside the Queue 
 * class.
 * - The compiler would have to know what a Node is.
 * ---- This could be alleviated two ways:
 * ------- The bad way would be to forward declare the Node class. This would nest the Node publicly, which we don't 
 *         want
 * ------- The other way is to just list all the private stuff on top, taking advantage of a class's default access. 
 *         This is perfectly reasonable to do
 * - More than the Node, I don't want to show users how the iterator works, just that it exists
 * - Similarly, I could have simply forward-declared the Node class privately and placed its definition "outside" 
 *   the Queue class as well
 */
template <typename T>
class Queue<T>::iterator {
public:
    iterator(Node* n);
    iterator& operator++();
    const iterator operator++(int);
    const T operator*();
    friend bool operator==(const Queue<T>::iterator &lhs, const Queue<T>::iterator &rhs)
    {
        return lhs.resource == rhs.resource;
    }

    friend bool operator!=(const Queue<T>::iterator &lhs, const Queue<T>::iterator &rhs)
    {
        return lhs.resource != rhs.resource;
    }
private:
    Node *resource;
};


// QUEUE CLASS
template <typename T>
Queue<T>::Queue()
    : _front(nullptr)
    , _back(nullptr)
    , _size(0)
{
}

template <typename T>
Queue<T>::Queue(T n)
    : _front(new Node(n))
    , _back(front)
    , _size(1)
{
}

template <typename T>
Queue<T>::~Queue()
{
    while (_front != _back) {
        Node* temp = _front;
        _front = _front->getNext();
        delete temp;
    }

    delete _front;
    _front = nullptr;
    _back = nullptr;
    _size = 0;
}

template <typename T>
typename Queue<T>::iterator Queue<T>::begin()
{
    return iterator(_front);
}

template <typename T>
typename Queue<T>::iterator Queue<T>::end()
{
    return iterator(nullptr);
}


template <typename T>
bool Queue<T>::empty() const
{
    return (_front == nullptr);
}

template <typename T>
unsigned int Queue<T>::size() const
{
    return _size;
}

template <typename T>
T& Queue<T>::front()
{
    return _front->getData();
}

template <typename T>
const T& Queue<T>::front() const
{
    return _front->getData();
}

template <typename T>
T& Queue<T>::back()
{
    return _back->getData();
}

template <typename T>
const T& Queue<T>::back() const
{
    return _back->getData();
}

template <typename T>
void Queue<T>::push(T n)
{
    Node *temp = new Node(n);

    if (_front == nullptr) {
        _front = temp;
        _back = _front;
        _size++;
        return;
    }

    _back->setNext(new Node(n));
    (_back->getNext())->setPrev(_back);
    _back = _back->getNext();
    _size++;
}

template <typename T>
void Queue<T>::pop()
{
    if (_front != nullptr) {
        Node *temp = _front;
        _front = _front->getNext();
        delete temp;
        _size--;
    }
}


// NODE CLASS
template <typename T>
Queue<T>::Node::Node(T n)
    : data(n)
    , next(nullptr)
    , prev(nullptr)
{
}

template <typename T>
T& Queue<T>::Node::getData()
{
    return data;
}

template <typename T>
const T Queue<T>::Node::getData() const
{
    return data;
}

template <typename T>
typename Queue<T>::Node::Node* Queue<T>::Node::getNext() const
{
    return next;
}

template <typename T>
typename Queue<T>::Node::Node* Queue<T>::Node::getPrev() const
{
    return prev;
}

template <typename T>
void Queue<T>::Node::setData(T d)
{
    data = d;
}

template <typename T>
void Queue<T>::Node::setNext(Node *n)
{
    next = n;
}

template <typename T>
void Queue<T>::Node::setPrev(Node *n)
{
    prev = n;
}


// ITERATOR CLASS
template <typename T>
Queue<T>::iterator::iterator(Node* n)
    : resource(n)
{
}

template <typename T>
typename Queue<T>::iterator::iterator& Queue<T>::iterator::operator++()
{
    if (resource != nullptr) {
        resource = resource->getNext();
    }

    return *this;
}

// Note the placement of const here with regards to typename. THIS IS THE CORRECT PLACEMENT
template <typename T>
const typename Queue<T>::iterator::iterator Queue<T>::iterator::operator++(int)
{
    iterator temp(resource);

    ++(*this);

    return temp;
}

template <typename T>
const T Queue<T>::iterator::operator*()
{
    return resource->getData();
}

#endif