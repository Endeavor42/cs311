#include <iostream>

int main()
{
    double a[10] = {0};
    double *p = a;

    // p = a;

    for (int i : a)
        std::cout << i << std::endl;
    
    std::cout << std::endl;
    
    for (int i = 0; i < 10; i++) {
        std::cout << (p + i) << ' ';
        *(p + i) = 5;
    }

    for (double i : a)
        std::cout << i << std::endl;


    return 0;
}